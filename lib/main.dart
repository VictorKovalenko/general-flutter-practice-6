import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter practice 6',
      theme: ThemeData(),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void datePicker() {
    showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(2020), lastDate: DateTime(2022)).then(
      (value) {
        if (value == null) {
          return;
        } else {
          print(MyDate.selectDate);
          setState(() {
            MyDate.selectDate = value;
            MyDate.date[0]=value;
          });
        }
      },
    );
  }

  String dayFormat(DateTime dateTime) {
    return 'Day:${dateTime.day} Month:${dateTime.month} Year:${dateTime.year}';
  }

  void timePicker() {
    var timePick = showTimePicker(context: context, initialTime: TimeOfDay.now()).then(
      (value) {
        if (value == null) {
          return;
        } else {
          print(value);
          setState(() {
            MyDate.selectTime = value;
            MyDate.date[1]=value;
          });
        }
      },
    );
  }

  String timeFormat(TimeOfDay timeOfDay) {
    return 'Hour:${MyDate.selectTime.hour} Minute:${MyDate.selectTime.minute}';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              DropdownButton<String>(
                value: MyDate.dropdownValue,
                onChanged: (String? newValue) {
                  setState(
                    () {
                      MyDate.dropdownValue = newValue!;
                      MyDate.date[3] = newValue;
                    },
                  );
                },
                items: <String>['Man', 'Woman'].map<DropdownMenuItem<String>>(
                  (String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  },
                ).toList(),
              ),
              Slider(
                value: MyDate.number,
                min: 0.0,
                max: 1.0,
                divisions: 1,
                label: MyDate.number.round().toString(),
                onChanged: (double value) {
                  setState(
                    () {
                      MyDate.number = value;
                      MyDate.date[2] = value;
                    },
                  );
                },
              ),
              Text('${dayFormat(MyDate.selectDate)}'),
              Center(
                child: Container(
                  child: ElevatedButton(
                    onPressed: () {
                      setState(() => datePicker());
                    },
                    child: Text('Date picker'),
                  ),
                ),
              ),
              const SizedBox(height: 50.0),
              Text('${timeFormat(MyDate.selectTime)}'),
              Container(
                child: ElevatedButton(
                  onPressed: () {
                    timePicker();
                  },
                  child: Text('Time picker'),
                ),
              ),
              const SizedBox(height: 50.0),
              ElevatedButton(
                onPressed: () {
                  MyDate.datePrint();
                },
                child: Text('Save'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class MyDate {
  static DateTime selectDate = DateTime.now();
  static var selectTime = TimeOfDay.now();
  static double number = 0.0;
  static String dropdownValue = 'Man';

  static Map<int, dynamic> date = {
    0: selectDate,
    1: selectTime,
    2: number,
    3: dropdownValue,
  };

  static void datePrint() {
    print(date);
  }
}
